[![pipeline status](https://gitlab.com/public_seidl/spring_examples/udemu_spring_5_guru/recipe_app_with_mongo_db/badges/master/pipeline.svg)](https://gitlab.com/public_seidl/spring_examples/udemu_spring_5_guru/recipe_app_with_mongo_db/commits/master)

[![coverage report](https://gitlab.com/public_seidl/spring_examples/udemu_spring_5_guru/recipe_app_with_mongo_db/badges/master/coverage.svg)](https://gitlab.com/public_seidl/spring_examples/udemu_spring_5_guru/recipe_app_with_mongo_db/commits/master)
# spring5-mongo-recipe-app
Recipe Application Using MongoDB

This repository is for an example application built in my Spring Framework 5 - Beginner to Guru

You can learn about my Spring Framework 5 Online course [here.](http://courses.springframework.guru/p/spring-framework-5-begginer-to-guru/?product_id=363173)