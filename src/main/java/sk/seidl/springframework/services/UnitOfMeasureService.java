package sk.seidl.springframework.services;

import reactor.core.publisher.Flux;
import sk.seidl.springframework.commands.UnitOfMeasureCommand;

/**
 * Created by jt on 6/28/17.
 */
public interface UnitOfMeasureService {

    //  Set<UnitOfMeasureCommand> listAllUoms();
    Flux<UnitOfMeasureCommand> listAllUoms();
}
