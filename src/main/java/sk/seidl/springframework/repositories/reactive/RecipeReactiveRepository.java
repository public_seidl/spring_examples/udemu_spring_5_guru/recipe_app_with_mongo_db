package sk.seidl.springframework.repositories.reactive;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import sk.seidl.springframework.domain.Recipe;

/**
 * @author Matus Seidl (5+3)
 * 2018-01-18
 */
@Repository
public interface RecipeReactiveRepository extends ReactiveMongoRepository<Recipe, String> {
}
